from django.shortcuts import render
from .models import Status
import datetime

# Create your views here.

def about(request):
    return render(request,"about.html")

def status(request):
    list_of_status = Status.objects.all().order_by('datetime')
    if request.method == 'POST':
        user_status = request.POST.get('status')
        user_time = datetime.datetime.now()
        status = Status(status=user_status, datetime=user_time)
        status.save()
        return render(request, 'status.html', {'list_status' : list_of_status})

    else:
        return render(request, "status.html", {'list_status' : list_of_status})
